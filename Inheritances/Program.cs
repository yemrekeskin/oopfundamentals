﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritances
{
    class Program
    {
        static void Main(string[] args)
        {
            // Sample: Use base class and derived types in a List.
            List<Net> nets = new List<Net>();
            nets.Add(new Perl());
            nets.Add(new Python());

            // Call virtual method on each instance.
            foreach (Net net in nets)
            {
                net.Act();
            }

            // Sample : Add three objects that implement the interface.
            var dictionary = new Dictionary<string, IValue>();
            dictionary.Add("cat1.png", new Image());
            dictionary.Add("image1.png", new Image());
            dictionary.Add("home.html", new Content());

            // Look up interface objects and call implementations.
            IValue value;
            if (dictionary.TryGetValue("cat1.png", out value))
            {
                value.Render(); // Image.Render
            }
            if (dictionary.TryGetValue("home.html", out value))
            {
                value.Render(); // Content.Render
            }

            // Sample
            B b = new B();
            b.Method1();



            Console.ReadLine();
        }
    }
}

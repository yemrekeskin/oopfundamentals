﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritances
{
    public class ClassA
    {
        public virtual void Doit()
        {

        }
    }

    public class ClassB
        : ClassA
    {
        sealed override public void Doit()
        {

            base.Doit();
        }
    }

    public class ClassC
        : ClassB
    {

    }
}

﻿using System;

namespace Inheritances
{
    internal interface IValue
    {
        void Render();
    }

    public class Content : IValue
    {
        public void Render()
        {
            Console.WriteLine("Render content");
        }
    }

    public class Image : IValue
    {
        public void Render()
        {
            Console.WriteLine("Render image");
        }
    }
}
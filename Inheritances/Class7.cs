﻿namespace Inheritances
{
    public class Class1
    {
        public void Say()
        {
        }
    }

    public class Class2
        : Class1
    {
        public new void Say()
        {
        }
    }

    public class Report
    {
        public string ReportCode { get; set; }
        public string ReportName { get; set; }

    }

    public interface IReportController
    {
        Report Extract();
    }

    public abstract class BaseReportController
        : IReportController
    {
        public abstract Report Extract();

        public virtual void Process()
        {
            // other operations
            this.Extract();
            // after to do
        }
    }

    public class ExcelReportController
        : BaseReportController
    {
        public override Report Extract()
        {
            throw new System.NotImplementedException();
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritances
{
    public abstract class X
    {
        public abstract void Method1();
    }

    public class Y : X
    {
        public override void Method1()
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;

namespace Inheritances
{
    // Base and Derived Classes
    internal class Net
    {
        public virtual void Act()
        {
        }
    }

    internal class Perl : Net
    {
        public override void Act()
        {
            Console.WriteLine("Perl.Act");
        }
    }

    internal class Python : Net
    {
        public override void Act()
        {
            Console.WriteLine("Python.Act");
        }
    }
}
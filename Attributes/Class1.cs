﻿using System;

namespace Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class KontrolAttribute
        : Attribute
    {
        public KontrolAttribute()
        {
            Console.WriteLine("Kontrol Attribute tetiklenmiştir.");
        }
    }

    /// <summary>
    /// Deneme
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class HelpAttribute
        : Attribute
    {
        public string Description { get; set; }

        public HelpAttribute()
        {
        }

        public HelpAttribute(string description)
        {
            
        }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class CheckAttribute 
        : Attribute
    {
        public int MaxLength { get; set; }
        public string Description { get; set; }
    }

    [Help]
    public class SampleClass
    {
        [Check(MaxLength = 25, Description = "Deneme")]
        public string Name { get; set; }
        
        [Obsolete]
        public void SampleMethod()
        {
        }
    }
}


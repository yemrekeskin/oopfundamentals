﻿using System;
using System.Reflection;

namespace Attributes
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            // Sample 
            Type type = typeof(NewClass);
            object[] attributes = type.GetCustomAttributes(false);

            foreach (var attribute in attributes)
            {
                Console.WriteLine("Usage attributes : {0}", attribute);
            }

            // Sample
            var attr = new ObsoleteAttribute();

            // Sample : Sistemde tanımlanan Kullanıcı Attribute larını listeler
            TypeInfo typeInfo = typeof(NewClass).GetTypeInfo();
            var attrs = typeInfo.GetCustomAttributes();

            foreach (var item in attrs)
            {
                Console.WriteLine("Attribute on MyClass: " + item.GetType().Name);
            }

            // Sample
            

            Console.ReadLine();
        }
    }

    [Description("")]
    public class NewClass
    {
        [Description("")]
        bool Doit()
        {
            return false;
        }
    }

    [AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = false)]
    public class DescriptionAttribute
        : Attribute
    {

        public string Description { get; set; }

        public DescriptionAttribute(string Description)
        {
            this.Description = Description;
        }
    }
}
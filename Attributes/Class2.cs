﻿using System;

namespace Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class ExcelSheetAttribute
        : Attribute
    {
        public string SheetName { get; set; }

        public ExcelSheetAttribute(string sheetName = "DefaultValue")
        {
            SheetName = sheetName;
        }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class ExcelColumnAttribute
        : Attribute
    {
        public string ColumnName { get; set; }
        public int Width { get; set; }

        public ExcelColumnAttribute(string columnName, int width)
        {
            ColumnName = columnName;
            Width = width;
        }
    }

    [ExcelSheet("NewSheet")]
    public class SavingAccount
    {
        [ExcelColumn("BALANCE", 50)]
        public string AccountName { get; set; }

        [ExcelColumn("BALANCE", 50)]
        public double Balance { get; set; }
    }
}
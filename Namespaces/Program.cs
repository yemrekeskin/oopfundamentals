﻿

extern alias Eski;
extern alias Yeni;

using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NewSqlClient = System.Data.SqlClient; // namespace aliasing
using NewSqlConnect = System.Data.SqlClient.SqlConnection; // class aliasing

using Namespaces = System.Data.OleDb; // takma adı ve namespace aynı isimde olursa değişken tanımlamak için :: operatörü kullanılır

using Console = System.Data.OleDb;

namespace Namespaces
{
    using sqlClient = System.Data.SqlClient; // namespace aliasing
    using sqlConnect = System.Data.SqlClient.SqlConnection; // class aliasing

    class Program
    {
        static void Main(string[] args)
        {
            // using ifadesini eklemeden
            System.Data.OleDb.OleDbCommand command = null;

            // using ifadeleri kullandığımda
            OleDbCommand sampleCommand = new OleDbCommand();

            Sample1.Class1 a;
            Sample2.Class1 b;

            sqlClient.SqlDataAdapter adapter = new sqlClient.SqlDataAdapter();
            sqlConnect.Equals(null, null);

            // namespace ve alias aynı isimde olur ise
            Namespaces::OleDbDataReader reader = null;


            // external aliasing
            Eski::MyNamespace.MyClass.MyMethod();
            Yeni::MyNamespace.MyClass.MyMethod();

            global::System.Console.ReadLine();
            
        }
    }
}

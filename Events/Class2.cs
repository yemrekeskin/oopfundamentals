﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events
{
    public interface IProcessor
    {
        void Process();
    }

    public class Processor
        : IProcessor
    {
        public delegate void BeforeProcessEventHandler();
        public delegate void AfterProcessEventHandler();

        public event BeforeProcessEventHandler OnBeforeProcess = null;
        public event AfterProcessEventHandler OnAfterProcess = null;

        public void Process()
        {
            // bir method referasns edilmiş mi ? edilmiş ise o method çalıştırılır
            if (OnBeforeProcess != null)
                OnBeforeProcess();

            Console.WriteLine("Process RUN");

            if (OnAfterProcess != null)
                OnAfterProcess();
        }
    }

}

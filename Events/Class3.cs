﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events
{
    public class Video
    {
        public string Title { get; set; }
    }

    public class VideoEncoder
    {
        public delegate void VideoEncodedEventHandler(object source, EventArgs arg);
        public event VideoEncodedEventHandler VideoEncoded = null;

        public void Encode(Video video)
        {
            // Encoding Logic

            OnVideoEncoded();
        }

        public void Decode(Byte[] data)
        {
            // Decoding Logic
        }

        protected virtual void OnVideoEncoded()
        {
            if(VideoEncoded != null)
            {
                VideoEncoded(this, EventArgs.Empty);
            }
        }
    }
}

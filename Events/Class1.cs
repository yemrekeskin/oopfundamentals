﻿namespace Events
{
    public delegate void ProductHandler(double oldPrice, double newPrice, Product product);

    public class Product
    {
        public event ProductHandler PriceChanged;

        public string Name { get; set; }

        private double price;

        public double Price
        {
            get { return price; }
            set
            {
                double oldPrice = price;
                price = value;

                if (PriceChanged != null)
                {
                    PriceChanged.Invoke(oldPrice, value, this);
                }
            }
        }
    }
}


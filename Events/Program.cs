﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events
{
    class Program
    {
        static void Main(string[] args)
        {
            // Sample
            Product p = new Product();
            p.PriceChanged += P_PriceChanged;

            p.Name = "NewProduct";

            p.Price = 12;
            p.Price = 24;

            // Sample
            Processor processor = new Processor();
            processor.OnBeforeProcess += Processor_OnBeforeProcess;
            processor.OnAfterProcess += Processor_OnAfterProcess;

            processor.Process();

            // Sample
            var video = new Video { Title = "New Video" };
            var videoEncoder = new VideoEncoder();

            videoEncoder.Encode(video);

            videoEncoder.VideoEncoded += VideoEncoder_VideoEncoded;

            Console.ReadLine();
        }

        private static void VideoEncoder_VideoEncoded(object source, EventArgs arg)
        {
            Console.WriteLine("Info:VideoEncoded");
        }

        private static void Processor_OnAfterProcess()
        {
            Console.WriteLine("Process After");
        }

        private static void Processor_OnBeforeProcess()
        {
            Console.WriteLine("Process Before");
        }

        private static void P_PriceChanged(double oldPrice, double newPrice, Product product)
        {
            Console.WriteLine("OldPrice {0} - NewPrice {1}", oldPrice, newPrice);
        }
    }
}

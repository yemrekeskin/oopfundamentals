﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events
{
    public class MailEventArgs 
        : EventArgs
    {
        public Mail Mail { get; set; }
    }

    public class Mail
    {

    }

    public class MessagingService
    {
        public delegate void MailSentEventHandler(object source, MailEventArgs args);
        public event MailSentEventHandler MailSent = null;

        public void SendMail(Mail mail)
        {
            // Send Mail

            OnMailSent(mail);
        }

        protected virtual void OnMailSent(Mail mail)
        {
            if(MailSent != null)
            {
                MailSent(this, new MailEventArgs() { Mail = mail });
            }
        }
    }
}

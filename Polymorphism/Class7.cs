﻿using System;

namespace Polymorphism
{
    public interface IMessageSender
    {
        void SendMessage();
    }

    public class EmailSender : IMessageSender
    {
        public void SendMessage()
        {
            Console.WriteLine(MessageText());
        }

        private string MessageText()
        {
            return "Message to send !";
        }
    }

    public class SmsSender : IMessageSender
    {
        public void SendMessage()
        {
            Console.WriteLine(MessageText());
        }

        private string MessageText()
        {
            return "Message to send !";
        }
    }
}

namespace NewNamespace
{
    public abstract class MessageSender
    {
        public abstract void SendMessage();

        protected string MessageText()
        {
            return "Message to send !";
        }
    }

    public class EmailSender : MessageSender
    {
        public override void SendMessage()
        {
            Console.WriteLine(MessageText());
        }
    }

    public class SmsSender : MessageSender
    {
        public override void SendMessage()
        {
            Console.WriteLine(MessageText());
        }
    }
}
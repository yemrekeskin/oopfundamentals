﻿namespace Polymorphism
{
    public class A
    {
        public virtual void DoWork()
        {
        }
    }

    public class B : A
    {
        public override void DoWork()
        {
        }
    }

    public class C : B
    {
        public sealed override void DoWork()
        {
        }
    }

    public class D : C
    {
        public new void DoWork() { }
    }
}
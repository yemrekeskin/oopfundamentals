﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    class Program
    {
        static void Main(string[] args)
        {
            //// interface polymorphism
            //// inheritance polymorphism

            //// Sample: Virtual Members
            //DerivedClass B = new DerivedClass();
            //B.DoWork();  

            //BaseClass A = (BaseClass)B;
            //A.DoWork();

            //// Sample: Hiding Base Class Members with New Members
            //Child C = new Child();
            //C.DoWork();  

            //Parent D = (Parent)C;
            //D.DoWork();

            //// Sample: Preventing Derived Classes from Overriding Virtual Members
            //// Sample: Accessing Base Class Virtual Members from Derived Classes -> base

            //// interface polymorphism
            //var car = new Car();
            //car.Start();
            //Start(car);

            //var truck = new Truck();
            //truck.Start();
            //Start(truck);

            //Stop(car);
            //Stop(truck);

            // interface polymorphisim
            Member objMember = new Member();

            ILive obj1 = (ILive)objMember;
            IHuman obj2 = (IHuman)objMember;

            obj1.Breath();
            obj2.Breath();

            var car = new Automobile();
            var motorcycle = new Motorcycle();

            Driver driver = new Driver(car);
            driver.Drive();

            var driver2 = new Driver(motorcycle);
            driver2.Drive();

            //

            var dog = new Dog();
            var cat = new Cat();

            TalkProvider provider = new TalkProvider();
            provider.Talk(dog);
            provider.Talk(cat);


            Console.ReadLine();
        }
        
        static void Start(Car car)
        {
            // logging
            // check another
            car.Start();
        }

        static void Start(Truck truck)
        {
            // logging
            // check another 
            truck.Start();
        }

        static void Stop(IMachine m)
        {
            m.Start();
        }

        // Method Overloading

        public static void Sum(int x, int y)
        {

        }

        public static void Sum(string x, string y)
        {

        }

        public static void Sum(decimal x, decimal y)
        {

        }


    }
}

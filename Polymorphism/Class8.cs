﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    public interface IHuman
    {
        void Breath();
    }

    public interface ILive
    {
        void Breath();
    }

    public class Member
        : IHuman,ILive
    {
        void IHuman.Breath()
        {
            Console.WriteLine("IHuman.Breath()");
        }

        void ILive.Breath()
        {
            Console.WriteLine("ILive.Breath()");
        }
    }
}

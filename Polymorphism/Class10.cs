﻿namespace Polymorphism
{
    public interface IVehicle
    {
        void Drive();
    }

    public class Automobile
        : IVehicle
    {
        public void Drive()
        {
            // drive operations
            // for car
        }
    }

    public class Motorcycle
        : IVehicle
    {
        public void Drive()
        {
            // drive operations
            // for motorcycles
        }
    }

    public class Driver
    {
        private readonly IVehicle Vehicle;

        public Driver(IVehicle vehicle)
        {
            this.Vehicle = vehicle;
        }

        public void Drive()
        {
            Vehicle.Drive();
        }
    }
}
﻿using System;

namespace Polymorphism
{
    public interface IStorable
    {
        void Load();

        void Save();
    }

    public class Document
        : IStorable
    {
        public void Load()
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }
    }

    public class GameObject
    {
        public virtual void Draw()
        {
            // generic draw operation
        }
    }

    public class Line
        : GameObject
    {
        public override void Draw()
        {
            base.Draw();
        }
    }

    public class Circle
        : GameObject
    {
        public override void Draw()
        {
            base.Draw();
        }
    }

    public class Square
        : GameObject
    {
        public override void Draw()
        {
            base.Draw();
        }
    }


}
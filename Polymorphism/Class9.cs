﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    public class Animal
    {
        public virtual string Talk()
        {
            return string.Empty;
        }
    }

    public class Cat
        : Animal
    {
        public override string Talk()
        {
            return base.Talk();
        }
    }

    public class Dog
        : Animal
    {
        public override string Talk()
        {
            return base.Talk();
        }
    }

    public class TalkProvider
    {
        public void Talk(Animal animal)
        {
            animal.Talk();
        }
    }
}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Partials
{
    // Method partial
    partial class Calculator
    {
        partial void OnChanged();

    }

    partial class Calculator
    {
        partial void OnChanged()
        {
            // method body  
        }
    }
}

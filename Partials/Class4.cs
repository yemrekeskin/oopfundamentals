﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Partials
{
    public partial class CoOrds
    {
        private int x;
        private int y;

        public CoOrds(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }

    public partial class CoOrds
    {
        public void PrintCoOrds()
        {
            Console.WriteLine("CoOrds: {0},{1}", x, y);
        }

    }
}

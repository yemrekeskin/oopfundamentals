﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Partials
{
    class Container
    {
        partial class Nested
        {
            void Test() { }
        }

        partial class Nested
        {
            void Test2() { }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Partials
{
    public interface IRotate { }

    public interface IRevolve { }
    public class Planet { }

    // Partial Class
    partial class Earth : Planet, IRotate { }
    partial class Earth : IRevolve { }

    // Normal Class
    public class Earth2 : Planet, IRotate, IRevolve
    {

    }

}

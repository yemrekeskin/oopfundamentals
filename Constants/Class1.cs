﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constants
{
    class Calendar1
    {
        public const int months = 12;
    }

    class Calendar2
    {
        const int months = 12, weeks = 52, days = 365;
    }

    class Calendar3
    {
        const int months = 12;
        const int weeks = 52;
        const int days = 365;

        const double daysPerWeek = (double)days / (double)weeks;
        const double daysPerMonth = (double)days / (double)months;
    }
}

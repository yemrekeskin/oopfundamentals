﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constants
{
    public class Class2
    {
        const double pi = 3.14;
        //const Tester test;

        readonly Int32 counter = 0;
        readonly Tester tester;

        public Class2()
        {
            // readonly sabitler sadece counstructor methodunda değiştirilebilir.
            // readonly sabitler object olabilirler

            // Const sabitler ilk değer ataması yapılmak zorundadır ve bir daha değiştirilmez.
            // Const tipi object olmaz

            counter++;
            //pi++;

        }
    }

    public class Tester
    {
        
    }

}

﻿using System;

namespace Methods
{
    public class PasswordGenerator
    {
        public string Get()
        {
            char[] chars = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                '!','@','?','$' };

            Random rnd = new Random();
            int randomNumber;

            string result = null;

            for (int i = 1; i <= 10; i++)
            {
                randomNumber = rnd.Next(0, chars.Length - 1);
                result += chars[randomNumber].ToString();
            }
            return result;
        }
    }
}
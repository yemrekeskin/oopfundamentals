﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methods
{
    class Program
    {
        static void Main(string[] args)
        {
            // Sample
            UseParams(1, 2, 3, 4);
            UseParams2(1, 'a', "test");
            
            // Sample
            TypeParser parser = new TypeParser();
            parser.Parse(1);
            parser.Parse("re");
            parser.Parse(false);


            Console.ReadLine();
        }

        public static void UseParams(params int[] list)
        {
            for (int i = 0; i < list.Length; i++)
            {
                Console.Write(list[i] + " ");
            }
            Console.WriteLine();
        }

        public static void UseParams2(params object[] list)
        {
            for (int i = 0; i < list.Length; i++)
            {
                Console.Write(list[i] + " ");
            }
            Console.WriteLine();
        }
    }
}

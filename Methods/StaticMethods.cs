﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methods
{
    public class LoggingService
    {
        public static void WriteToFile(params string[] changedItems)
        {
            foreach (var item in changedItems)
            {
                Console.WriteLine(item);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methods
{
    // Method Overloading
    public class TypeParser
    {
        public string Parse(string value)
        {
            return String.Empty;
        }

        public string Parse(int value)
        {
            return String.Empty;
        }

        public string Parse(Decimal value)
        {
            return String.Empty;
        }

        public string Parse(Boolean value)
        {
            return String.Empty;
        }
    }
}

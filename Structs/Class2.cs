﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Structs
{
    public struct Books
    {
        public string title;
        public string author;
        public string subject;
        public int book_id;
    };

    public class Magazines
    {
        public string title;
        public string author;
        public string subject;
        public int book_id;
    };
}

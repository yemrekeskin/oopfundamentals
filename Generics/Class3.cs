﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics
{
    public interface IParser
    {
        object Parse(string input);

        bool TryParse(string input, out object result);
    }

    public interface IParser<T>
    {
        T Parse(string input);

        bool TryParse(string input, out T result);
    }

    public abstract class ParserBase<T> : IParser, IParser<T>
    {
        object IParser.Parse(string input)
        {
            return (object)Parse(input);
        }

        bool IParser.TryParse(string input, out object result)
        {
            T obj;

            if (TryParse(input, out obj))
            {
                result = obj;
                return true;
            }

            result = default(T);
            return false;
        }
        

        #region IParserBase implementation

        public abstract T Parse(string input);

        public abstract bool TryParse(string input, out T result);

        #endregion

    }

    public class BoolParser : ParserBase<bool>
    {

        public override bool Parse(string input)
        {
            return bool.Parse(input);
        }

        public override bool TryParse(string input, out bool result)
        {
            return bool.TryParse(input, out result);
        }

    }

    public class DecimalParser : ParserBase<decimal>
    {

        public override decimal Parse(string input)
        {
            return decimal.Parse(input);
        }

        public override bool TryParse(string input, out decimal result)
        {
            return decimal.TryParse(input, out result);
        }

    }
}

﻿namespace Generics
{
    internal class BaseNode
    { }

    internal class BaseNodeGeneric<T>
    { }

    // concrete type
    internal class NodeConcrete<T> : BaseNode
    { }

    //closed constructed type
    internal class NodeClosed<T> : BaseNodeGeneric<int>
    { }

    //open constructed type
    internal class NodeOpen<T> : BaseNodeGeneric<T>
    { }

    internal class BaseNodeMultiple<T, U>
    { }

    //No error
    internal class Node4<T> : BaseNodeMultiple<T, int>
    { }

    //No error
    internal class Node5<T, U> : BaseNodeMultiple<T, U>
    { }
}
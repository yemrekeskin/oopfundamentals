﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics
{
    class Program
    {
        static void Main(string[] args)
        {
            // Sample
            SampleGeneric<string> sampleObject = new SampleGeneric<string>();
            sampleObject.Field = "Sample string";

            SampleGeneric<int> sampleObject2 = new SampleGeneric<int>
            {
                Field = 2
            };

            // Sample

            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Sample.CourseNET
{
    public class Instructor
    {
        public DateTime HireDate { get; set; }

        public virtual ICollection<Course> Courses { get; set; }
        public virtual OfficeAssignment OfficeAssignment { get; set; }
    }
}
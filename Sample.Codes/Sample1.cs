﻿using System;

namespace Sample.Codes
{
    public abstract class DataProcessor
    {
        public abstract void SetDataSource(string url);

        public abstract bool IsValid();

        public abstract void Parse();

        public abstract void SaveData();

        // template method
        public virtual void GetResult()
        {
            if (IsValid())
            {
                Parse();
                SaveData();
            }
        }
    }

    public class LinkedinDataProcessor
        : DataProcessor
    {
        public override void SetDataSource(string url)
        {
            throw new NotImplementedException();
        }

        public override bool IsValid()
        {
            throw new NotImplementedException();
        }

        public override void Parse()
        {
            throw new NotImplementedException();
        }

        public override void SaveData()
        {
            throw new NotImplementedException();
        }

        public override void GetResult()
        {
            // Extra Data Process
            base.GetResult();
        }
    }

    public class TwitterDataProcessor
        : DataProcessor
    {
        public override void SetDataSource(string url)
        {
            throw new NotImplementedException();
        }

        public override bool IsValid()
        {
            throw new NotImplementedException();
        }

        public override void Parse()
        {
            throw new NotImplementedException();
        }

        public override void SaveData()
        {
            throw new NotImplementedException();
        }
    }

    public class GithubDataProcessor
        : DataProcessor
    {
        public override void SetDataSource(string url)
        {
            throw new NotImplementedException();
        }

        public override bool IsValid()
        {
            throw new NotImplementedException();
        }

        public override void Parse()
        {
            throw new NotImplementedException();
        }

        public override void SaveData()
        {
            throw new NotImplementedException();
        }
    }

    public class BitbucketDataProcessor
        : DataProcessor
    {
        public override void SetDataSource(string url)
        {
            throw new NotImplementedException();
        }

        public override bool IsValid()
        {
            throw new NotImplementedException();
        }

        public override void Parse()
        {
            throw new NotImplementedException();
        }

        public override void SaveData()
        {
            throw new NotImplementedException();
        }
    }
}
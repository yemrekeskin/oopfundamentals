﻿using System;
using System.Text.RegularExpressions;

namespace Sample.Codes
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //(xxx)xxx-xxxx: (123)456-7890
            //(xxx) xxx-xxxx: (123) 456-7890
            //xxx-xxx-xxxx: 123-456-7890
            //xxxxxxxxxx: 1234567890

            string c1 = "(123)456-7890";
            string c2 = "(123) 456-7890";
            string c3 = "123-456-7890";
            string c4 = "1234567890";

            if (c1.IsValidPhone()) Console.WriteLine("Correct :D"); else Console.WriteLine("Wrong :O");
            if (c2.IsValidPhone()) Console.WriteLine("Correct :D"); else Console.WriteLine("Wrong :O");
            if (c3.IsValidPhone()) Console.WriteLine("Correct :D"); else Console.WriteLine("Wrong :O");
            if (c4.IsValidPhone()) Console.WriteLine("Correct :D"); else Console.WriteLine("Wrong :O");

            Console.WriteLine();

            //xxxxx-xxxx: 01234-5678
            //xxxxx: 01234

            string z1 = "01234-5678";
            string z2 = "01234";

            if (z1.IsValidZip()) Console.WriteLine("Correct :D"); else Console.WriteLine("Wrong :O");
            if (z2.IsValidZip()) Console.WriteLine("Correct :D"); else Console.WriteLine("Wrong :O");

            Console.WriteLine();

            string dummy = "()h{e??l#'l>>o<<";
            dummy = dummy.RemoveNonAlphaNumericCharacters();

            Console.WriteLine(dummy);

            Console.WriteLine();

            Console.ReadKey();
        }
    }

    /// <summary>
    /// Extention Method --> This Keyword
    /// </summary>
    public static class ValidExtention
    {
        public static bool IsValidPhone(this string input)
        {
            return Regex.IsMatch(input, @"^\(?\d{3}\)?[\s\-]?\d{3}\-?\d{4}$");
        }

        public static bool IsValidZip(this string input)
        {
            return Regex.IsMatch(input, @"^\d{5}(\-\d{4})?$");
        }

        public static string RemoveNonAlphaNumericCharacters(this string input)
        {
            return Regex.Replace(input, @"[^\w\.@-]", string.Empty);
        }
    }
}
﻿namespace Sample.Codes
{
    public class TestProcessorOutput
    {
        public string ScreenTestOutputMessage { get; set; }
        public string StoredProcedureTestOutputMessage { get; set; }
        public string RunOutPutFileValidationTestOutputMessage { get; set; }
        public string RunInputFilevalidationTestOutputMessage { get; set; }
    }

    public interface ITestProcessor
    {
        ITestProcessor RunScreenTests();

        ITestProcessor RunStoredProcedureTests();

        ITestProcessor RunOutPutFileValidationTests();

        ITestProcessor RunInputFilevalidationTests();

        TestProcessorOutput TakeResults();
    }

    public class TestProcessor
            : ITestProcessor
    {
        public TestProcessorOutput TestOutput { get; private set; }

        public TestProcessor(TestProcessorOutput testOutput)
        {
            this.TestOutput = testOutput;
        }

        public ITestProcessor RunScreenTests()
        {
            // testleri çalıştırır
            this.TestOutput.ScreenTestOutputMessage = "";
            return this;
        }

        public ITestProcessor RunStoredProcedureTests()
        {
            // testleri çalıştırır
            this.TestOutput.StoredProcedureTestOutputMessage = "";
            return this;
        }

        public ITestProcessor RunOutPutFileValidationTests()
        {
            // testleri çalıştırır
            this.TestOutput.RunOutPutFileValidationTestOutputMessage = "";
            return this;
        }

        public ITestProcessor RunInputFilevalidationTests()
        {
            // testleri çalıştırır
            this.TestOutput.RunInputFilevalidationTestOutputMessage = "";
            return this;
        }

        public TestProcessorOutput TakeResults()
        {
            return this.TestOutput;
        }
    }

    public static class FluentTestFactory
    {
        public static ITestProcessor Init()
        {
            return new TestProcessor(new TestProcessorOutput());
        }
    }

    // using of it
    //var testOutputs = FluentTestFactory
    //                        .Init()
    //                        .RunInputFilevalidationTests()
    //                        .RunOutPutFileValidationTests()
    //                        .RunScreenTests()
    //                        .RunStoredProcedureTests()
    //                        .TakeResults();
}
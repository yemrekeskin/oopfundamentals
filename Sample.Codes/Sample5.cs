﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.Codes
{
    public class Payment
    {

    }

    public class BaseRepository<T>
    {

    }

    public class DomesticPaymentRepository
        : BaseRepository<Payment>
    {

    }

    public class ForeignPaymentRepository
        : BaseRepository<Payment>
    {

    }

    public class TreasuryRepository
        : BaseRepository<Payment>
    {

    }

    public class UniversalRepository
    {
        // repositories
        private DomesticPaymentRepository _domestic;
        private ForeignPaymentRepository _foreign;
        private TreasuryRepository _treasury;

        public DomesticPaymentRepository DomesticPaymentRepository
        {
            get
            {
                if (null == _domestic)
                    _domestic = new DomesticPaymentRepository();
                return _domestic;
            }
        }

        public ForeignPaymentRepository ForeignPaymentRepository
        {
            get
            {
                if (null == _foreign)
                    _foreign = new ForeignPaymentRepository();
                return _foreign;
            }
        }

        public TreasuryRepository TreasuryRepository
        {
            get
            {
                if (null == _treasury)
                    _treasury = new TreasuryRepository();
                return _treasury;
            }
        }
    }
}

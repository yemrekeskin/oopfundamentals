﻿using System;

namespace Sample.Codes
{
    public interface IEntity
    {
    }

    public delegate void RuleEventHandler(object sender, RuleEventArgs e);

    public class RuleEventArgs
        : EventArgs
    {
        public RuleEventArgs()
            : base()
        { }
    }

    public interface IRule
    {
        void Execute();

        //void Execute(object sender, RuleEventArgs e);

        bool RuleCondition();

        event RuleEventHandler BeginExecution;

        void OnBeginExecution(object sender);

        event RuleEventHandler EndExecution;

        void OnEndExecution(object sender);

        event RuleEventHandler PassedRule;

        void OnPassedRule(object sender);

        event RuleEventHandler FailedRule;

        void OnFailedRule(object sender);
    }

    public abstract class BaseRule
        : IRule
    {
        private string rulename;

        public string RuleName
        {
            get { return rulename; }
        }

        // Payment object(FACT) to use in RuleCondition method
        public readonly IEntity _payment;

        public BaseRule(string rulename, IEntity payment)
        {
            this.rulename = rulename;
            this._payment = payment;
        }

        //public virtual void Execute();
        //public virtual void Execute(object sender, RuleEventArgs e);
        public virtual bool RuleCondition()
        {
            throw new NullReferenceException();
        }

        public virtual void Execute()
        {
            OnBeginExecution(this);

            if (RuleCondition())
                OnPassedRule(this);
            else OnFailedRule(this);

            OnEndExecution(this);
        }

        public event RuleEventHandler BeginExecution;

        public void OnBeginExecution(object sender)
        {
            if (BeginExecution != null)
                BeginExecution(sender, new RuleEventArgs());
        }

        public event RuleEventHandler EndExecution;

        public void OnEndExecution(object sender)
        {
            if (EndExecution != null)
                EndExecution(sender, new RuleEventArgs());
        }

        public event RuleEventHandler PassedRule;

        public void OnPassedRule(object sender)
        {
            if (PassedRule != null)
                PassedRule(sender, new RuleEventArgs());
        }

        public event RuleEventHandler FailedRule;

        public void OnFailedRule(object sender)
        {
            if (FailedRule != null)
                FailedRule(sender, new RuleEventArgs());
        }
    }
}
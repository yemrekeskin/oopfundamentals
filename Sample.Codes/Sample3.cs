﻿using System;
using System.IO;
using System.Net;

namespace Sample.Codes
{
    public interface ILogger
    {
        void Write(Exception ex);

        void Write(string logText);
    }

    public abstract class BaseLogger
        : ILogger
    {
        public static string logFilePath = string.Empty;

        public string LogFilePath
        {
            set { logFilePath = value; }
            get { return logFilePath; }
        }

        private static StreamWriter sw = null;

        private static bool WriteLog(string strPathName, Exception objException)
        {
            bool bReturn = false;
            string strException = string.Empty;
            try
            {
                sw = new StreamWriter(strPathName, true);
                sw.WriteLine("Source        : " + objException.Source.ToString().Trim());
                sw.WriteLine("Method        : " + objException.TargetSite.Name.ToString());
                sw.WriteLine("Date          : " + DateTime.Now.ToLongTimeString());
                sw.WriteLine("Time          : " + DateTime.Now.ToShortDateString());
                sw.WriteLine("Computer      : " + Dns.GetHostName().ToString());
                sw.WriteLine("Error         : " + objException.Message.ToString().Trim());
                sw.WriteLine("Stack Trace   : " + objException.StackTrace.ToString().Trim());
                sw.WriteLine("-----------------------------------------------------");
                sw.Flush();
                sw.Close();
                bReturn = true;
            }
            catch (Exception)
            {
                bReturn = false;
            }
            return bReturn;
        }

        private static bool WriteLog(string strPathName, string logText)
        {
            bool bReturn = false;
            string strException = string.Empty;
            try
            {
                sw = new StreamWriter(strPathName, true);
                sw.WriteLine("LogText       : " + logText.Trim());
                sw.WriteLine("Date          : " + DateTime.Now.ToLongTimeString());
                sw.WriteLine("Time          : " + DateTime.Now.ToShortDateString());
                sw.WriteLine("Computer      : " + Dns.GetHostName().ToString());
                sw.WriteLine("-----------------------------------------------------");
                sw.Flush();
                sw.Close();
                bReturn = true;
            }
            catch (Exception)
            {
                bReturn = false;
            }
            return bReturn;
        }

        public static bool ErrorRoutine(Exception objException)
        {
            string strPathName = string.Empty;

            if (logFilePath.Equals(string.Empty))
            {
                strPathName = GetLogFilePath();
            }
            else
            {
                if (true != File.Exists(logFilePath))
                {
                    if (false == CheckDirectory(logFilePath))
                        return false;

                    FileStream fs = new FileStream(logFilePath,
                            FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    fs.Close();
                }
                strPathName = logFilePath;
            }
            bool bReturn = true;
            
            if (true != WriteLog(strPathName, objException))
            {
                bReturn = false;
            }
            return bReturn;
        }

        public static bool ErrorRoutine(string logText)
        {
            string strPathName = string.Empty;

            if (logFilePath.Equals(string.Empty))
            {
                strPathName = GetLogFilePath();
            }
            else
            {
                if (true != File.Exists(logFilePath))
                {
                    if (false == CheckDirectory(logFilePath))
                        return false;

                    FileStream fs = new FileStream(logFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    fs.Close();
                }
                strPathName = logFilePath;
            }
            bool bReturn = true;
            
            if (true != WriteLog(strPathName, logText))
            {
                bReturn = false;
            }
            return bReturn;
        }

        private static string GetLogFilePath()
        {
            try
            {
                //string retFilePath = ConfigurationSettings.AppSettings["LogFile"].ToString();
                string retFilePath = @"C:\\Logs\LogDaily.log";
                logFilePath = retFilePath;
                
                if (File.Exists(retFilePath) == true)
                    return retFilePath;
                else
                {
                    if (false == CheckDirectory(logFilePath))
                        return string.Empty;

                    FileStream fs = new FileStream(retFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    fs.Close();
                }

                return retFilePath;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        private static bool CheckDirectory(string strLogPath)
        {
            try
            {
                string strDirectoryname = Path.GetDirectoryName(strLogPath);

                if (false == Directory.Exists(strDirectoryname))
                    Directory.CreateDirectory(strDirectoryname);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public virtual void Write(Exception ex)
        {
            // Default
        }

        public virtual void Write(string logText)
        {
            // Default
        }
    }

    public class TxtFileLogger
        : BaseLogger
    {
        public override void Write(Exception ex)
        {
            ErrorRoutine(ex);
            base.Write(ex);
        }

        public override void Write(string logText)
        {
            ErrorRoutine(logText);
            base.Write(logText);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.Codes
{
    /// <summary>
    /// Validation provider by luhn Algorithm
    /// </summary>
    public static class CreditCardNumberExtention
    {
        public const int DIGIT_NUM = 16;

        public static bool IsValidCreditCardNumber(this string accountNumber)
        {
            if (String.IsNullOrEmpty(accountNumber))
                throw new FormatException("EmptyAccNumber");

            if (DIGIT_NUM != accountNumber.Length)
                throw new FormatException("AccNumberMaxDigit");

            int singleIndexValue = 0;
            int doubleIndexValue = 0;

            for (int i = 0; i < accountNumber.Length; i++)
            {
                int indexValue = int.Parse(accountNumber[i].ToString());
                if (i % 2 == 0)
                    doubleIndexValue += SumDigit(indexValue * 2);
                else
                    singleIndexValue += indexValue;
            }

            return (singleIndexValue + doubleIndexValue) % 10 == 0 ? true : false;
        }

        private static int SumDigit(int value)
        {
            int sum = 0;
            while (value > 0)
            {
                sum += value % 10;
                value /= 10;
            }
            return sum;
        }
    }
}

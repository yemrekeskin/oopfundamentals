﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace Sample.Codes
{

    public class SystemDbs
    {
        public const string MsSqlDao = "";
        public const string OracleDao = "";
        public const string DAOFactory = "";

    }
    public class Account
    {
        // set fields and properties
    }

    public class Customer
    {
        // set fields and properties
    }

    public interface IAccount
    {
        int CreateAccount(Account account);

        bool DeleteAccount(int accountId);

        bool UpdateAccount(Account account);

        List<Account> GetAccount();
    }

    public interface ICustomer
    {
        int CreateCustomer(Customer customer);

        bool DeleteCustomer(int customerId);

        bool UpdateCustomer(Customer customer);

        List<Customer> GetCustomers();

        bool DeleteAccounts(int customerID);
    }

    // Data Account Object for sql connect
    public class SqlAccountDAO
        : IAccount
    {
        public int CreateAccount(Account account)
        {
            throw new NotImplementedException();
        }

        public bool DeleteAccount(int accountId)
        {
            throw new NotImplementedException();
        }

        public bool UpdateAccount(Account account)
        {
            throw new NotImplementedException();
        }

        public List<Account> GetAccount()
        {
            throw new NotImplementedException();
        }
    }

    public class SqlCustomerDAO
        : ICustomer
    {
        public int CreateCustomer(Customer customer)
        {
            throw new NotImplementedException();
        }

        public bool DeleteCustomer(int customerId)
        {
            throw new NotImplementedException();
        }

        public bool UpdateCustomer(Customer customer)
        {
            throw new NotImplementedException();
        }

        public List<Customer> GetCustomers()
        {
            throw new NotImplementedException();
        }

        public bool DeleteAccounts(int customerID)
        {
            throw new NotImplementedException();
        }
    }

    // Data Account Object for oracle connect
    public class OracleAccountDAO
        : IAccount
    {
        public int CreateAccount(Account account)
        {
            throw new NotImplementedException();
        }

        public bool DeleteAccount(int accountId)
        {
            throw new NotImplementedException();
        }

        public bool UpdateAccount(Account account)
        {
            throw new NotImplementedException();
        }

        public List<Account> GetAccount()
        {
            throw new NotImplementedException();
        }
    }

    public class OracleCustomerDAO
        : ICustomer
    {
        public int CreateCustomer(Customer customer)
        {
            throw new NotImplementedException();
        }

        public bool DeleteCustomer(int customerId)
        {
            throw new NotImplementedException();
        }

        public bool UpdateCustomer(Customer customer)
        {
            throw new NotImplementedException();
        }

        public List<Customer> GetCustomers()
        {
            throw new NotImplementedException();
        }

        public bool DeleteAccounts(int customerID)
        {
            throw new NotImplementedException();
        }
    }

    // Data Access Object Factory
    public abstract class DAOFactory
    {
        public abstract ICustomer GetCustomerDao();

        public abstract IAccount GetAccountDao();

        public static DAOFactory GetDaoFactory()
        {
            string strDaoFactoryType = SystemDbs.DAOFactory;
            if (String.IsNullOrEmpty(strDaoFactoryType))
            {
                throw new NullReferenceException("");
            }
                
            else
            {
                Type DaoFactoryType = Type.GetType(strDaoFactoryType);
                if (DaoFactoryType == null)
                    throw new NullReferenceException("");

                Type DaoFactoryBaseType =
                        Type.GetType("Sample.DataAccessLayer.DAOFactory");
                if (!DaoFactoryBaseType.IsAssignableFrom(DaoFactoryType))
                    throw new ArgumentException("");

                DAOFactory daoFactory = (DAOFactory)Activator.CreateInstance(DaoFactoryType);
                return daoFactory;
            }
        }
    }

    public class SqlDAOFactory
        : DAOFactory
    {
        public static string ConnectionString
        {
            get
            {
                if (SystemDbs.MsSqlDao == null)
                {
                    throw new NullReferenceException("");
                }

                string connectionString = SystemDbs.MsSqlDao;
                if (String.IsNullOrEmpty(connectionString))
                    throw new NullReferenceException("");
                else
                    return connectionString;
            }
        }

        public override ICustomer GetCustomerDao()
        {
            return new SqlCustomerDAO();
        }

        public override IAccount GetAccountDao()
        {
            return new SqlAccountDAO();
        }
    }

    public class OracleDAOFactory
        : DAOFactory
    {
        public static string ConnectionString
        {
            get
            {
                if (SystemDbs.OracleDao == null)
                {
                    throw new NullReferenceException("");
                }

                string connectionString = SystemDbs.OracleDao;
                if (String.IsNullOrEmpty(connectionString))
                    throw new NullReferenceException("");
                else
                    return connectionString;
            }
        }

        public override ICustomer GetCustomerDao()
        {
            throw new NotImplementedException();
        }

        public override IAccount GetAccountDao()
        {
            throw new NotImplementedException();
        }
    }
}
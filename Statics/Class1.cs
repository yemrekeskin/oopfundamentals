﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Statics
{
    /// <summary>
    /// Extention Method
    /// </summary>
    public static class TypeExtensions
    {
        public static T ChangeTypeTo<T>(this object obj) where T : IConvertible
        {
            if (obj == null)
                return default(T);

            try
            {
                return (T)Convert.ChangeType(obj, typeof(T));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

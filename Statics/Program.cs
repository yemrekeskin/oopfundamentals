﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Statics
{
    static class SampleClass
    {
        public static string SampleString = "Sample String";
    }

    class Program
    {
        static void Main(string[] args)
        {
            // Sample
            Console.WriteLine(SampleClass.SampleString);

            // Sample
            var size = 100;
            AssertHelper.IsTrue(size > 0);
            AssertHelper.AreEqual(15, 10);


            Console.ReadLine();
        }
    }


}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.Blog
{
    public enum CommentState 
        : byte
    {
        WaitingApproval = 0,
        Approved = 1,
        Rejected = 2,
        Deleted = 3
    }
}

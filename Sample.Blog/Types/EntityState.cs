﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.Blog
{
    public enum EntityState 
        : byte
    {
        /// <summary>
        /// Active
        /// </summary>
        Active = 0,

        /// <summary>
        /// Passive
        /// </summary>
        Passive = 1
    }
}

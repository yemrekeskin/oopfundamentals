﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.Blog
{
    public class File
        : Entity
    {
        public Post Post { get; set; }
        public Page Page { get; set; }

        public string FileName { get; set; }
        public string ContentType { get; set; }

        public byte[] Content { get; set; }
    }
}

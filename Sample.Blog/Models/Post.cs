﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.Blog
{
    public class Post
        : Entity
    {
        public ICollection<Tag> Tags { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<File> Files { get; set; }

        public Category Category { get; set; }

        public Post()
        {
            Tags = new HashSet<Tag>();
            Comments = new HashSet<Comment>();
            Files = new HashSet<File>();
        }

        public string Header { get; set; }
        public string Summary { get; set; }
        public string Content { get; set; }
    }
}

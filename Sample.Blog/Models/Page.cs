﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.Blog
{
    public class Page
        : Entity
    {
        public ICollection<Tag> Tags { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<File> Files { get; set; }

        public Page()
        {
            Tags = new HashSet<Tag>();
            Comments = new HashSet<Comment>();
            Files = new HashSet<File>();
        }

        public string Header { get; set; }
        public string Content { get; set; }

        public string MetaAuthor { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDescription { get; set; }
    }
}

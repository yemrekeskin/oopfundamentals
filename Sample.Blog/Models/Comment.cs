﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.Blog
{
    public class Comment
        : Entity
    {
        public Page Page { get; set; }
        public Post Post { get; set; }

        public string Email { get; set; }
        public string Description { get; set; }

        public CommentState CommentState { get; set; }
        

    }
}

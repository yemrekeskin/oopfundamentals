﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.Blog
{
    public class User
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Photo { get; set; }
        public string Bio { get; set; }

        public string Country { get; set; }
        public string City { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.Blog
{
    public class Category
       : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public ICollection<Post> Posts { get; set; }

        public Category()
        {
            Posts = new HashSet<Post>();
        }
    }
}

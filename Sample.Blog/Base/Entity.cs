﻿using System;

namespace Sample.Blog
{
    public interface IEntity
    {
    }

    public abstract class Entity
        : IEntity
    {
        public int Id { get; set; }

        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }

        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }

        public bool IsDeleted { get; set; }

        public EntityState State { get; set; }
        
    }
}

﻿namespace Interfaces
{
    internal interface IValue
    {
        int Count { get; set; } // Property interface.
        string Name { get; set; } // Property interface.
    }

    internal class Image : IValue // Implements interface.
    {
        public int Count // Property implementation.
        {
            get;
            set;
        }

        private string _name;

        public string Name // Property implementation.
        {
            get { return this._name; }
            set { this._name = value; }
        }
    }

    internal class Article : IValue // Implements interface.
    {
        public int Count // Property implementation.
        {
            get;
            set;
        }

        private string _name;

        public string Name // Property implementation.
        {
            get { return this._name; }
            set { this._name = value.ToUpper(); }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    interface ISampleInterface
    {
        void DoSomething();
    }

    class SampleClass 
        : ISampleInterface
    {

        // ?
        //void ISampleInterface.doSomething()
        //{
        //    // Method implementation.  
        //}

        public void DoSomething()
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface ILoggable
    {
        string Log();
    }
    
    public class Product : ILoggable
    {
        public Product()
        {

        }

        public Product(int productId)
        {
            this.ProductId = productId;
        }

        public decimal? CurrentPrice { get; set; }
        public int ProductId { get; private set; }
        public string ProductDescription { get; set; }
        
        public string Log()
        {
            var logString = this.ProductId + ": " +
                            "Detail: " + this.ProductDescription;
            return logString;
        }
    }
}

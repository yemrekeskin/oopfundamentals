﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface ICategoryRepository
    {
        void List();
    }

    public interface IGroupRepository
    {
        void List();
    }

    public class Class5
       : ICategoryRepository, IGroupRepository
    {

        void ICategoryRepository.List()
        {
            throw new NotImplementedException();
        }

        void IGroupRepository.List()
        {
            throw new NotImplementedException();
        }
    }
}

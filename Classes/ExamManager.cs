﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes
{
    public class ExamManager
    {
        public int ExamId { get; set; }

        public int ExamCode { get; set; }
        public string ExamName { get; set; }

        public ExamManager()
        {

        }

        public ExamManager(int ExamCode)
        {
            this.ExamCode = ExamCode;
        }

    }
}

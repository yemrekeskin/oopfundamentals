﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes
{
    public class Account
    {
        public string AccountType { get; set; }
        public int AccountBranch { get; set; }
        public int AccountNumber { get; set; }
        public int AccountSuffix { get; set; }

        // Constructor : ctor tab tab
        public Account()
        {

        }

        public Account(string accountType, int accountBranch, int accountNumber, int accountSuffix)
        {
            this.AccountType = accountType; // this ?
            AccountBranch = accountBranch;
            AccountNumber = accountNumber;
            AccountSuffix = accountSuffix;
        }

        // Destructor :  AltGr + Ü
        ~Account()
        {

        }
    }
}

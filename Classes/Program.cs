﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes
{
    class Program
    {
        static void Main(string[] args)
        {
            // Sample: Create Object new , ()
            Customer object1 = new Customer();

            Customer object3 = new Customer();
            Customer object4 = object3;

            // Sample : Create Object with different ways
            Account account1 = new Account();
            account1.AccountType = "1";
            account1.AccountBranch = 9900;
            account1.AccountNumber = 55001;
            account1.AccountSuffix = 001;

            Account account2 = new Account
            {
                AccountType = "1",
                AccountBranch = 9900,
                AccountNumber = 55001,
                AccountSuffix = 001
            };

            Account account3 = new Account("1", 9900, 55001, 001);

            // Sample : Constructor method overloading
            ExamManager manager = new ExamManager();
            manager.ExamCode = 101;
            

            Console.ReadLine();
        }
    }
}

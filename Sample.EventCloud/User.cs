﻿using System;

namespace Sample.EventCloud
{
    public class User
    {
        public long Id { get; set; }
        public Guid UniqueKey { get; set; }

        public string Name { get; set; }
        public string Surname { get; set; }
        public string LoginName { get; set; }

        public string FullName => String.Format("{0} {1}", this.Name, this.Surname);

        //public string FullName
        //{
        //    get
        //    {
        //        return String.Format("{0} {1}", this.Name, this.Surname);
        //    }
        //}

        public User()
        {
            this.UniqueKey = Guid.NewGuid();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.EventCloud
{
    public interface IEventManager
    {
        bool CreateEvent(Event @event);
        bool CancelEvent(Event @event);
        
        bool Register(Registration register);
        bool CancelRegister(Registration register);
                
    }

    public class EventManager
        : IEventManager
    {
        public bool CancelEvent(Event @event)
        {
            throw new NotImplementedException();
        }

        public bool CancelRegister(Registration register)
        {
            throw new NotImplementedException();
        }

        public bool CreateEvent(Event @event)
        {
            if (Valid(@event))
            {
                // create event
                // notifications : email,sms
            }

            return true;
        }

        public bool Register(Registration register)
        {
            throw new NotImplementedException();
        }

        bool Valid(Event @event)
        {
            bool isValid = false;

            if (String.IsNullOrEmpty(@event.Description))
                isValid = false;

            // ....

            return isValid;
        }

        bool Valid(Registration register)
        {
            bool isValid = false;

            // ....

            return isValid;
        }
    }
}

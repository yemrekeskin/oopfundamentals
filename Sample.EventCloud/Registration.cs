﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.EventCloud
{
    public class Registration
    {
        public User User { get; set; }
        public Event Event { get; set; }
        public DateTime RegistrationDate { get; set; }

        public Registration()
        {
            this.RegistrationDate = DateTime.Now;
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Sample.EventCloud
{
    public class Event
    {
        public string Title { get; protected set; }        
        public string Description { get; protected set; }
        public DateTime Date { get; protected set; }
        public bool IsCancelled { get; protected set; }        
        public int MaxRegistrationCount { get; protected set; }        
        public ICollection<Registration> Registrations { get; protected set; }
        public DateTime CreatedDate { get; set; }
        
        public Event()
        {
            // Set Default Values
            this.MaxRegistrationCount = SystemConstants.MaxRegistrationCount;
            this.IsCancelled = false;
            this.CreatedDate = DateTime.Now;
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Sample.IKBoard
{
    public interface ICompanyRepository
    {
        bool Add(Company company);

        bool Update(Company company);

        bool Delete(Company company);

        Company Get(Company company);

        List<Company> List();
    }

    public class CompanyRepository
        : ICompanyRepository
    {
        public bool Add(Company company)
        {
            var result = false;
            if (company.IsValid())
            {
                // save
                result = true;
            }
            return result;
        }

        public bool Delete(Company company)
        {
            throw new NotImplementedException();
        }

        public Company Get(Company company)
        {
            throw new NotImplementedException();
        }

        public List<Company> List()
        {
            throw new NotImplementedException();
        }

        public bool Update(Company company)
        {
            throw new NotImplementedException();
        }
    }
}
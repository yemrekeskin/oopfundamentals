﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.IKBoard
{
    public class Resume
        : BaseModel, ITracing
    {
        public Profile Profile { get; set; }

        public List<Contact> Contacts { get; set; }
        public List<Language>  Languages { get; set; }
        public List<Reference> References { get; set; }
        public List<Experience> Experiences { get; set; }
        public List<Certification> Certifications { get; set; }
        public List<File> Files { get; set; }


        #region Tracing Properties
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool IsDeleted { get; set; }

        public override bool IsValid()
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}

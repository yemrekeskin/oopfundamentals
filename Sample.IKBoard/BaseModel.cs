﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.IKBoard
{
    public interface IModel
    {

    }

    public abstract class BaseModel
        : IModel
    {
        public int Id { get; set; }

        public Guid UniqueId { get; set; }
        
        public BaseModel()
        {
            this.UniqueId = Guid.NewGuid();
        }

        public abstract bool IsValid();        
    }
}

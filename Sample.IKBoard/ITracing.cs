﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.IKBoard
{
    public interface ITracing
    {
        string CreateBy { get; set; }
        DateTime CreateDate { get; set; }

        string UpdateBy { get; set; }
        DateTime UpdateDate { get; set; }

        bool IsDeleted { get; set; }
    }
}

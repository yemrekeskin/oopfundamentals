﻿using System;

namespace Encapsulation
{
    internal class MyClass
    {
        protected string MyName = "YunusEmre";
        private string MySurName = "Keskin";

        public MyClass()
        {
            this.MySurName = String.Empty;
        }
    }

    class ChildClass
        : MyClass
    {
        public String GetName()
        {
            return MyName;
        }
    }
}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encapsulation
{
    public class BankAccount
    {
        public void OpenAccount()
        {

        }

        public string CloseAccount()
        {
            var result = "Success:YouAccountClosed";
            if(this.CheckBalance())
            {
                this.CalculateInterest();
                this.ApplyPenalties();
                this.GetTax();
                this.BaseCloseAccount();
            }
            else
            {
                result = "Error:YouAccountDidNotClose";
            }

            return result;
        }
        
        protected virtual void CalculateInterest()
        {

        }

        protected virtual void ApplyPenalties()
        {

        }

        protected virtual void GetTax()
        {

        }

        private void BaseCloseAccount()
        {

        }

        protected bool CheckBalance()
        {
            return true;
        }
    }

    public class SavingAccount 
        : BankAccount
    {
        protected override void ApplyPenalties()
        {
            base.ApplyPenalties();
        }

        protected override void CalculateInterest()
        {
            base.CalculateInterest();
        }

        protected override void GetTax()
        {
            base.GetTax();
        }
    }

    public class CheckingAccount
        : BankAccount
    {
        protected override void ApplyPenalties()
        {
            base.ApplyPenalties();
        }

        protected override void CalculateInterest()
        {
            base.CalculateInterest();
        }

        protected override void GetTax()
        {
            base.GetTax();
        }
    }
}

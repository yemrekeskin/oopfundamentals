﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encapsulation
{
    public class ServerProvider
    {
        public string ServerInstance { get; set; }
        public string Login { get; set; }
        public string Pass { get; set; }
        public string Database { get; set; }

        public ServerProvider(string serverInstance, string login, string pass, string database)
        {
            if (String.IsNullOrEmpty(this.ServerInstance))
                throw new Exception("Error:ServerInstanceIsEmpty");

            if (String.IsNullOrEmpty(this.Login))
                throw new Exception("Error:LoginIsEmpty");

            if (String.IsNullOrEmpty(this.Pass))
                throw new Exception("Error:PassIsEmpty");

            this.ServerInstance = serverInstance;
            this.Login = login;
            this.Pass = pass;
            this.Database = database;
        }

        public void Initial()
        {
            this.InitialAuth(ServerInstance, Login, Pass);
            this.InitialConnection(Database);
            this.InitialParams();
        }

        private void InitialAuth(string serverInstance, string login, string pass)
        {
            
        }

        private void InitialParams()
        {

        }

        private void InitialConnection(string database)
        {

        }
    }
}

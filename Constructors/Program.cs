﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructors
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Console Started");

            // Sample : Çok fazla nesne oluşturarak Garbage COllector un devreye girmesini sağlayalım
            Question main = new Question();
            int counter = 1000;
            for (int i = 0; i < counter; i++)
            {
                main.Generate(); 
            }

            // Sample : Disposable
            using (Worker worker = new Worker())
            {
                // Use worker object in code here.
            }

            Console.WriteLine();
            
            using (Resources ex = new Resources())
            {
                ex.ShowDuration();
            }
            

            Console.WriteLine("Console Finished");
            Console.ReadLine();
        }
    }
    
}

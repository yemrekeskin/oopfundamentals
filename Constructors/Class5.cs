﻿using System;
using System.Diagnostics;

namespace Constructors
{
    public class Resources
        : IDisposable
    {
        private Stopwatch sw;

        public Resources()
        {
            sw = Stopwatch.StartNew();
            Console.WriteLine("Instantiated object");
        }

        public void ShowDuration()
        {
            Console.WriteLine("This instance of {0} has been in existence for {1}",
                              this, sw.Elapsed);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        ~Resources()
        {
            Console.WriteLine("Finalizing object");
            sw.Stop();
            Console.WriteLine("This instance of {0} has been in existence for {1}",
                              this, sw.Elapsed);
        }
    }
}
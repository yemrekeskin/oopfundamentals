﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructors
{
    public class Worker
        : IDisposable
    {
        private bool alreadyDisposed = false;

        public Worker()
        {
            Console.WriteLine("In the constructor.");
        }

        public void Dispose(bool explicitCall)
        {
            if (!this.alreadyDisposed)
            {
                if (explicitCall)
                {
                    System.Console.WriteLine("Not in the destructor, " +
                     "so cleaning up other objects.");
                    // Not in the destructor, so we can reference other objects.
                    //OtherObject1.Dispose();
                    //OtherObject2.Dispose();
                }
                // Perform standard cleanup here...
                Console.WriteLine("Cleaning up.");
            }
            alreadyDisposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        

        ~Worker()
        {
            Console.WriteLine("In the destructor now.");
            Dispose(false);
        }
    }
}

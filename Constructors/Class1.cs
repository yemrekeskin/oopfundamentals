﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructors
{
    class First
    {
        public First()
        {
            Trace.WriteLine("First's constructor is called.");
        }

        ~First()
        {
            Trace.WriteLine("First's destructor is called.");
        }
    }

    class Four
        : IDisposable
    {
        public Four()
        {
            Console.Write("Four- Constructor");
        }

        ~Four()
        {
            Console.Write("Four- Destructor");
        }

        public void Dispose()
        {

        }
    }

    class Second : First
    {
        ~Second()
        {
            Trace.WriteLine("Second's destructor is called.");
        }
    }

    class Third : Second
    {
        ~Third()
        {
            Trace.WriteLine("Third's destructor is called.");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructors
{
    public class Question
    {
        public void Generate()
        {
            Question obj = new Question();
        }

        ~Question()
        {
            Console.WriteLine("Destructor Object");
        }

        //protected override void Finalize()
        //{
        //    try
        //    {
        //        // Your code
        //    }
        //    finally
        //    {
        //        base.Finalize();
        //    }
        //}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructors
{
    public class DisposableObject 
        : IDisposable
{
        private bool disposed;

        ~DisposableObject()
        {
            this.Dispose(false);
        }

        public void Dispose()
        {
            if (!this.disposed)
            {
                this.Dispose(true);
                GC.SuppressFinalize(this);
                this.disposed = true;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // clean up managed resources
            }

            // clean up unmanaged resources
        }
    }

    /// <summary>
    /// Aliasing the IDisposable interface
    /// </summary>
    public class ClosableObject : IDisposable
    {
        private bool closed;

        ~ClosableObject()
        {
            this.Dispose(false);
        }

        public void Close()
        {
            if (!this.closed)
            {
                this.Dispose(true);
                GC.SuppressFinalize(this);
                this.closed = true;
            }
        }

        void IDisposable.Dispose()
        {
            this.Close();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // clean up managed resources
            }

            // clean up unmanaged resources
        }
    }
}

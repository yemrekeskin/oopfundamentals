﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nesteds
{
    public class DataTable
    {
        // Nested Type 
        public class Rows
        {
            private int _Count;
            public int Count
            {
                get
                {
                    return _Count;
                }
            }
        }

        // Nested Type
        public class Columns
        {
            private string _Caption;
            public string Caption
            {
                get
                {
                    return _Caption;
                }
                set
                {
                    _Caption = value;
                }
            }
        }
    }
}


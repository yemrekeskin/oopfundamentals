﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indexers
{
    public class Person
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }

        public override string ToString()
        {
            return string.Format("Name: {0} Surname: {1} Age: {2}"
                , Name, Surname, Age);
        }
    }

    public class PersonCollection : IEnumerable
    {
        private ArrayList arPeople = new ArrayList();

        //ÖNEMLİ!! bu sayede PersonCollection'ı
        //index operatoru [] ile kullanabilecegiz
        public Person this[int index]
        {
            get { return (Person)arPeople[index]; }
            set { arPeople.Insert(index, value); }
        }

        public int Count
        {
            get { return arPeople.Count; }
        }

        public IEnumerator GetEnumerator()
        {
            return arPeople.GetEnumerator();
        }
    }

    class PersonDictionaryCollection : IEnumerable
    {
        private Dictionary<string, Person> arPeople
                   = new Dictionary<string, Person>();
        //ÖNEMLİ!! artık isim üzerinden index
        //yapiliyor
        public Person this[string name]
        {
            get { return (Person)arPeople[name]; }
            set { arPeople[name] = value; }
        }

        public int Count
        {
            get { return arPeople.Count; }
        }

        public IEnumerator GetEnumerator()
        {
            return arPeople.GetEnumerator();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indexers
{
    class Members
    {
        private readonly string[] _members = { "Yunus", "Emre", "Keskin" };

        public string this[int index]
        {
            get { return _members[index]; }
            set { value = _members[index]; }
        }
        
    }
}

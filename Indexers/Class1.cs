﻿using System;

namespace Indexers
{
    public class SampleCollection<T>
    {
        private T[] arr = new T[100];
        
        //public T this[int i] => arr[i];

        public T this[int i]
        {
            get { return arr[i]; }
            set { arr[i] = value; }
        }

        //public T this[int i]
        //{
        //    get => arr[i];
        //    set => arr[i] = value;
        //}

        int nextIndex = 0;
        public void Add(T value)
        {
            if (nextIndex >= arr.Length)
                throw new IndexOutOfRangeException($"The collection can hold only {arr.Length} elements.");
            arr[nextIndex++] = value;
        }

        public int Count { get { return arr.Length; } }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indexers
{
    class Farm
    {
        string[] _animals = new string[10];
        public string this[int number]
        {
            get
            {
                return _animals[number];
            }
            set
            {
                _animals[number] = value;
            }
        }
    }
}

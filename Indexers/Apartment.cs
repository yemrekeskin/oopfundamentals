﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indexers
{
    class Apartment
    {
        string[] _furniture = new string[10];

        public string this[int number]
        {
            get => _furniture[number]; // Expression-bodied indexer.
            set => _furniture[number] = value;
        }
    }
}

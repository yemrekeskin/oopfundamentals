﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indexers
{
    class Program
    {
        static void Main(string[] args)
        {
            // Indexer : indexer tab tab

            // Sample
            Layout layout = new Layout();
            layout[1] = "Frank Gehry";
            layout[3] = "I. M. Pei";
            layout[10] = "Frank Lloyd Wright";
            layout[11] = "Apollodorus";
            layout[-1] = "Error";
            layout[1000] = "Error";

            // Read elements through the indexer.
            string value1 = layout[1];
            string value2 = layout[3];
            string value3 = layout[10];
            string value4 = layout[11];
            string value5 = layout[50];
            string value6 = layout[-1];

            // Write the results.
            Console.WriteLine(value1);
            Console.WriteLine(value2);
            Console.WriteLine(value3);
            Console.WriteLine(value4);
            Console.WriteLine(value5); // Is null
            Console.WriteLine(value6);

            // Sample
            // ... Use an index initializer.
            Farm f = new Farm() { [1] = "cat", [3] = "bird", [5] = "dog" };
            // Get values from farm by index.
            Console.WriteLine(f[1]);
            Console.WriteLine(f[3]);
            Console.WriteLine(f[5]);


            // Sample
            var apt = new Apartment() { [0] = "chair", [1] = "bed", [2] = "desk" };
            // Use indexer.
            Console.WriteLine(apt[0]);
            Console.WriteLine(apt[1]);
            Console.WriteLine(apt[2]);

            // Sample : Generic class and indexer
            SampleCollection<string> colllector = new SampleCollection<string>();
            colllector[1] = "New Record";
            colllector[2] = "Old Record";

            Console.WriteLine(colllector[0]);
            Console.WriteLine(colllector.Count);

            Console.ReadLine();
        }
    }
}

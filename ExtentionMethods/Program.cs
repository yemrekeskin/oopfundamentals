﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtentionMethods
{
    class Program
    {
        static void Main(string[] args)
        {
            // Sample
            var value = "YunusEmreKeskin";
            Console.WriteLine(value.InsertSpaces());

            var result = "InterTech".InsertSpaces();
            Console.WriteLine(result);

            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Sample.TaskNET.Repository;

namespace Sample.TaskNET.Service
{
    public interface IProjectService
    {
        bool AddProject(ProjectViewModel viewModel);
        bool UpdateProject(ProjectViewModel viewModel);
    }

    public class ProjectService
        : IProjectService
    {
        private readonly ProjectRepository repo;
        public ProjectService(ProjectRepository repo)
        {
            this.repo = repo;       
        }

        public ProjectService()
        {

        }

        public bool AddProject(ProjectViewModel viewModel)
        {
            var returnValue = false;

            // Convert
            var model = this.Convert(viewModel);

            // Validation ?

            // Business Workflow

            // Add to Database
            this.repo.AddProject(model);

            return returnValue;
        }

        public bool UpdateProject(ProjectViewModel viewModel)
        {
            // Convert
            // Validation ?
            // Business Workflow
            // Update to Database

            throw new NotImplementedException();
        }

        private Project Convert(ProjectViewModel viewModel)
        {
            var result = new Project();
            result.ProjectCode = viewModel.ProjectCode;
            result.ProjectName = viewModel.ProjectName;
            result.StartDate = viewModel.StartDate;
            result.EndDate = viewModel.EndDate;

            return result;
        }
    }
}

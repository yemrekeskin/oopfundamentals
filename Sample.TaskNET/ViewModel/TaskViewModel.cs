﻿using System;

namespace Sample.TaskNET
{
    public class TaskViewModel
        : ViewModel
    {
        public Person AssignedPerson { get; set; }
        
        public int? AssignedPersonId { get; set; }

        /// <summary>
        /// Describes the task.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The time when this task is created.
        /// </summary>
        public DateTime CreationTime { get; set; }

        /// <summary>
        /// Current state of the task.
        /// </summary>
        public TaskState State { get; set; }

        /// <summary>
        /// Default costructor.
        /// Assigns some default values to properties.
        /// </summary>
        public TaskViewModel()
        {
            CreationTime = DateTime.Now;
            State = TaskState.Active;
        }
    }
}

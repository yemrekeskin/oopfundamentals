﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.TaskNET.Repository
{
    public interface IProjectRepository
    {
        bool AddProject(Project model);
        bool UpdateProject(Project model);
    }

    public class ProjectRepository
        : IProjectRepository
    {
        public bool AddProject(Project model)
        {
            // add database

            throw new NotImplementedException();
        }

        public bool UpdateProject(Project model)
        {
            // update database
            throw new NotImplementedException();
        }
    }
}

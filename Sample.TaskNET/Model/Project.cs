﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.TaskNET
{
    public class Project
    {
        public string ProjectCode { get; set; }

        public string ProjectName { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

    }
}

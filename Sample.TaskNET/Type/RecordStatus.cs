﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.TaskNET
{
    public enum RecordStatus
    {
        /// <summary>
        /// No action required.
        /// </summary>
        None = 0,

        /// <summary>
        /// New record has been added.
        /// </summary>
        New = 1,

        /// <summary>
        /// Record has been updated.
        /// </summary>
        Updated = 2,

        /// <summary>
        /// Record has been deleted.
        /// </summary>
        Deleted = 3
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.TaskNET
{
    public enum TaskState 
        : byte
    {
        /// <summary>
        /// The task is active.
        /// </summary>
        Active = 1,

        /// <summary>
        /// The task is completed.
        /// </summary>
        Completed = 2
    }
}

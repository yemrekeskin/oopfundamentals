﻿namespace Sample.Reporter
{
    public class Table
    {
        public string Schema { get; set; }
        public string Name { get; set; }

        public TableColumn Columns { get; set; }

    }
}
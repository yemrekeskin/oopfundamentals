﻿using System;

namespace Sample.Reporter
{
    public interface IBaseReport
    {

    }
    
    public abstract class BaseReport
        : IBaseReport
    {
        public string ReportCode { get; set; }

        public string ReportName { get; set; }
        
        public Table Table { get; set; }

        public string CreatedDate { get; set; }

    }
    
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Properties
{
    public class Person
    {
        private string firstName;
        private string lastName;

        public Person(string first, string last)
        {
            firstName = first;
            lastName = last;
        }

        public string Name => $"{firstName} {lastName}";
    }

    class Medication
    {
        private static int test;
        public static int Test { get => test; set => test = value; }

        public int Quantity { get; set; } = 30; // Has default value.
    }

    public class Example
    {
        static string _backing; // Backing store for property.
        static string Property // Getter and setter.
        {
            get
            {
                return _backing;
            }
            set
            {
                _backing = value;
            }
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Properties
{
    public class Counter
    {
        static int _count;
        public static int Count
        {
            get
            {
                // Side effect of this property.
                _count++;
                return _count;
            }
        }
    }
}

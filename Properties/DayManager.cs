﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Properties
{
    class DayManager
    {
        DayOfWeek _day;
        public DayOfWeek Day
        {
            get
            {
                // We don't allow this to be used on Friday.
                if (this._day == DayOfWeek.Friday)
                {
                    throw new Exception("Invalid access");
                }
                return this._day;
            }
            set
            {
                this._day = value;
            }
        }
    }
}

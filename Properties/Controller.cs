﻿namespace Properties
{
    public class Controller
    {
        public Controller()
        {
            this.IsFound = true;
        }

        private bool _found;

        public bool IsFound
        {
            get
            {
                return this._found;
            }
            private set
            {
                this._found = value;
            }
        }


    }
}
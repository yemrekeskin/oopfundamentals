﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Properties
{
    class Program
    {
        static void Main(string[] args)
        {
            // Sample
            Product product = new Product();
            product.Number = 5; // set { }
            Console.WriteLine(product.Number); // get { }

            // Sample
            DayManager manager = new DayManager();
            manager.Day = DayOfWeek.Monday;
            Console.WriteLine(manager.Day == DayOfWeek.Monday);
            
            // Sample 
            Customer customer = new Customer();

            // Sample
            Controller controller = new Controller();
            Console.WriteLine(controller.IsFound);

            // Sample
            var count = Counter.Count;
            Console.WriteLine(count);
            Console.WriteLine(Counter.Count);
            Console.WriteLine(Counter.Count);


            Console.ReadLine();
        }
    }
}

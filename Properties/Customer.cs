﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Properties
{
    public class Customer
    {
        /* Properties*/
        public int CustomerId { get; private set; }

        public int CustomerCode { get; protected set; }


        public string Name { get; set; }
        public string SurName { get; set; }

        public int Age { get; }

        //public string EmailAddress { set; }
        private string _emailAddress = null;

        public string EmailAddress
        {
            get
            {
                // Any codes
                return this._emailAddress;
            }
            set
            {
                // Any Codes
                value = this._emailAddress;
            }
        }

        public string FullName
        {
            get
            {
                string fullName = Name;
                if (!string.IsNullOrWhiteSpace(Name))
                {
                    if (!string.IsNullOrWhiteSpace(fullName))
                    {
                        fullName += ", ";
                    }
                    fullName += SurName;
                }
                return fullName;
            }
        }

        public static int Level { get; set; }
    }
}

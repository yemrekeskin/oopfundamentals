﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    public class MethodClass
    {
        public void Method1(string message) { }
        public void Method2(string message) { }
    }

    public class NewProgram
    {
        public NewProgram()
        {
            MethodClass obj = new MethodClass();
            Del d1 = obj.Method1;
            Del d2 = obj.Method2;
            Del d3 = DelegateMethod;

            //Both types of assignment are valid.
            Del allMethodsDelegate = d1 + d2;
            allMethodsDelegate += d3;

            allMethodsDelegate.Invoke("Hello Motto");
        }

        public static void DelegateMethod(string message)
        {
            System.Console.WriteLine(message);
        }
    }
}

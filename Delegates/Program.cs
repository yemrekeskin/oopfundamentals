﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

delegate void Temsilci(); 

namespace Delegates
{
    delegate void Sum(); 
    
    public class Program
    {
        static void Main(string[] args)
        {
            // oluşturma 
            Temsilci t = new Temsilci(Deneme);
            t.Invoke();


            // Ekleme
            var sample = new Sample();
            t += new Temsilci(sample.Test);
            t.Invoke();


            // çıkarmak
            t -= new Temsilci(Deneme);
            t.Invoke();

            // Delege oluştuturken new kullanmayabiliriz
            Sum sums = Deneme;
            sums += sample.Test;
            sums.Invoke();

            Console.ReadLine();
        }

        public static void Deneme()
        {
            Console.WriteLine("Deneme Method Invoke");
        }
    }

    public class Sample
    {
        public void Test()
        {
            Console.WriteLine("Test Method Invoke");
        }
    }
}

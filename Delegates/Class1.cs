﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    public delegate int PerformCalculation(int x, int y);
    public delegate void Del(string message);

    public class Class1
    {

        public Class1()
        {
            // Instantiate the delegate.
            Del handler = DelegateMethod;
            Func<>
            // Call the delegate.
            handler("Hello World");
        }

        // Create a method for a delegate.
        public static void DelegateMethod(string message)
        {
            Console.WriteLine(message);
        }
    }
}

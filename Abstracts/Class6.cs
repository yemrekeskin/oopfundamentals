﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstracts
{
    public abstract class BaseMachine
    {

    }

    public abstract class BaseAutomobile
        : BaseMachine
    {

    }

    public class Automobile
        : BaseAutomobile
    {

    }


    /*  Struct */


    public interface IVehicle
    {

    }

    public struct Vehicle
        : IVehicle
    {


    }
}

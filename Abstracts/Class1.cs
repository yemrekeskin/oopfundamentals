﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstracts
{
    public class Context
    {

    }


    public enum AccountingFlow 
        : byte
    {

    }

    public abstract class BaseAction
    {
        public abstract AccountingFlow ExecuteNew(Context ctx);
        public abstract AccountingFlow ExecuteUpdate(Context ctx);
        public abstract AccountingFlow ExecuteDelete(Context ctx);
    }


    public class DataImportAction
        : BaseAction
    {
        public override AccountingFlow ExecuteDelete(Context ctx)
        {
            throw new NotImplementedException();
        }

        public override AccountingFlow ExecuteNew(Context ctx)
        {
            throw new NotImplementedException();
        }

        public override AccountingFlow ExecuteUpdate(Context ctx)
        {
            throw new NotImplementedException();
        }
    }

    public class DataExportAction
        : BaseAction
    {
        public override AccountingFlow ExecuteDelete(Context ctx)
        {
            throw new NotImplementedException();
        }

        public override AccountingFlow ExecuteNew(Context ctx)
        {
            throw new NotImplementedException();
        }

        public override AccountingFlow ExecuteUpdate(Context ctx)
        {
            throw new NotImplementedException();
        }
    }

}

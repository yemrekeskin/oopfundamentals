﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstracts
{
    public class Request { }
    public class Response { }

    public interface IMessagingProcessor
    {
        Response Execute(Request request);
    }

    public abstract class MessagingProcessor
        : IMessagingProcessor
    {
        public virtual Response Execute(Request request)
        {
            throw new NotImplementedException();
        }
    }

    public class SystemProcessor
        : MessagingProcessor
    {


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectRelations
{
    public class Department
    {
        public string DepartmentName { get; set; }

        public List<Teacher> Teachers { get; set; }
    }

    public class Teacher
    {
        public string TeacherName { get; set; }
    }
    

}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectRelations
{
    public class Bar
    {

    }

    //Association - I have a relationship with an object. Foo uses Bar
    public class Foo
    {
        void Baz(Bar bar)
        {

        }
    };

    // Composition - I own an object and I am responsible for its lifetime, when Foo dies, so does Bar
    public class Foo2
    {
        private Bar bar = new Bar();
    }

    // Aggregation - I have an object which I've borrowed from someone else. When Foo dies, Bar may live on.
    public class Foo3
    {
        private Bar bar;
        Foo3(Bar bar)
        {
            this.bar = bar;
        }
    }
}

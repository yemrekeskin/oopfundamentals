﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectRelations
{
    public class SomeUtilityClass
    {
        public void DoSomething()
        {

        }
    }

    // Association --> I have a relationship with an object
    // Parser uses SomeUtilityClass
    public class Parser
    {
        public void doSomething(SomeUtilityClass obj)
        {
            obj.DoSomething();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectRelations
{
    public class Vehicle
    {

    }

    // Inheritance --> is-a relation
    // parent-child relation
    // Car is a Vehicle 
    public class Car
        : Vehicle
    {
        public Car()
        {
            // Composition
            Engine engine = new Engine();
        }
    }

    // Composition
    // Engine is-a-part-of Car
    public class Engine
    {

    }
}

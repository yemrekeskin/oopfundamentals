﻿using System.Web;

namespace Enums
{
    public class ApplicationHelper
    {
        public static ApplicationTypes GetApplicationType()
        {
            if (HttpRuntime.AppDomainAppId != null)
            {
                return ApplicationTypes.Web;
            }

            return ApplicationTypes.Desktop;
        }

        public enum ApplicationTypes
        {
            Web,
            Desktop
        }
    }
}
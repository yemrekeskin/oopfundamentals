﻿using System;

namespace Enums
{
    // A traditional enumeration of some root vegetables.
    public enum SomeRootVegetables
    {
        HorseRadish,
        Radish,
        Turnip
    }

    // A bit field or flag enumeration of harvesting seasons.
    [Flags]
    public enum Seasons
    {
        None = 0,
        Summer = 1,
        Autumn = 2,
        Winter = 4,
        Spring = 8,
        All = Summer | Autumn | Winter | Spring
    }

    public enum CommentState
        : byte
    {
        WaitingApproval = 0,
        Approved = 1,
        Rejected = 2,
        Deleted = 3
    }

    public enum StateOptions
        : byte
    {
        /// <summary>
        /// Active
        /// </summary>
        Active = 0,

        /// <summary>
        /// Passive
        /// </summary>
        Passive = 1
    }
}

﻿using System;

namespace TestConsole
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var i = 10;
            // method argüman olarak göndermekden önce değişkenin ilk değerinin olması gerekir,
            Topla(ref i);
            Console.WriteLine(i);

            // ilk değer set edilmiyor ise out kullanılır

            int value;
            Increment(out value);
            Console.WriteLine(value);

            Console.ReadLine();            
        }

        public static void Topla(ref int value)
        {
            value++;
        }

        public static void Increment(out int value)
        {
            value = 10;
        }
    }
}
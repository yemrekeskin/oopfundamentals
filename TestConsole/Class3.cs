﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestConsole
{
    public class Teacher
    {
        public List<Student> Students { get; set; }

    }

    public class Student
    {
        public List<Teacher> Teachers { get; set; }

    }
}

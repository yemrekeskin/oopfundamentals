﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestConsole
{
    public interface IMessagingProcessor
    {

    }

    public abstract class BaseMessagingProcessor
        : IMessagingProcessor
    {


    }

    public class SystemMessagingProcessor
        : BaseMessagingProcessor
    {

    }
}
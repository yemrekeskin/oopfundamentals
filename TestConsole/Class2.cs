﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestConsole
{
    public class Department
    {
        public int Id { get; set; }

        public DepartmentType DepartmentType { get; set; }

        public string DepartmentName { get; set; }
    }    

    public enum DepartmentType
    {
        HumanResources = 0,
        Sales = 1,
        Marketing = 2,
        Advertisement = 3
    }

}






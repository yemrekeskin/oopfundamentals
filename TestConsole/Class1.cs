﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestConsole
{
    public interface ILogger
    {
        void Handle(string msg);
    }

    public class FileLogger
        : ILogger
    {
        public void Handle(string msg)
        {
            Console.WriteLine("FileLogger");
        }
    }

    public class ConsoleLogger
        : ILogger
    {
        public void Handle(string msg)
        {
            Console.WriteLine("ConsoleLogger");
        }
    }

    public class Log
    {
        private List<ILogger> listener = new List<ILogger>();

        public void RegisterLogger(ILogger logger)
        {
            listener.Add(logger);
        }

        public void WriteLog(string msg)
        {
            foreach (ILogger logger in listener)
            {
                logger.Handle(msg);
            }
        }
    }

}

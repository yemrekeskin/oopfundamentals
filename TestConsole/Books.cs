﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestConsole
{
    public struct Books
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string Subject { get; set; }

    }
}